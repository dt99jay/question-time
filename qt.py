import requests
import mwparserfromhell
import pandas as pd
from collections import Counter
from matplotlib import pyplot as plt

parties = ["Labour Party (UK)", "Scottish Conservative Party", "Liberal Democrats (UK)", "Conservative Party (UK)", "Ulster Unionist Party", "Liberal Party (UK)", "Labour Party (Ireland)", "Plaid Cymru", "Conservative Party (United Kingdom)", "Social Democratic Party (UK)", "Socialist Workers Party (UK)", "Welsh Labour", "Free Democratic Party (Germany)", "Social Democratic and Labour Party", "Social Democratic Party (UK, 1988)", "Scottish National Party", "UK Independence Party", "Fine Gael", "Communist Party of Great Britain", "Republican Party (United States)", "Democratic Party (United States)", "Co-operative Party", "Labour Co-operative", "Scottish Conservative and Unionist Party", "Women's Equality Party", "Sinn Féin", "Welsh Liberal Democrats", "UK Unionist Party", "Progressive Unionist Party", "Fianna Fáil", "Australian Labor Party", "Australian Democrats", "Liberal Party of Australia", "Solidarity (Scotland)", "Green Party of England and Wales", "Democratic Unionist Party", "Solidarity–People Before Profit", "Pakistan Peoples Party", "Scottish Socialist Party", "Welsh Conservative Party", "United Kingdom Conservative Party", "The Republicans (France)", "Movement for Democratic Change – Tsvangirai", "Liberal Democrats UK", "Labour and Co-operative", "Scottish Liberal Democrats", "Referendum Party", "Scottish Labour Party", "National Front (UK)", "Traditional Unionist Voice", "Scottish Green Party", "Independent (politics)", "Scottish Conservatives", "Conservative Party(UK)", "Syriza", "Labour Party (United Kingdom)", "Kadima", "European People's Party", "Party of European Socialists"] # Used to estimate party in absence of an infobox with 'party' param (these are parties of past QT panellists, so some are a bit random)

party_groups = {"Con": "Conservative", "Lab": "Labour", "UKIP": "UK Independence Party", "Lib Dem": "Liberal Democrats", "SNP": "Scottish National Party", "Plaid": "Plaid", "Green": "Green"} # Used to group parties e.g. Scottish Conservative Party and Conservative Party (UK)

party_colours = {"Con": "#0087DC", "Lab": "#D50000", "UKIP": "#B3009D", "Lib Dem": "#FDBB30", "SNP": "#FFF95D", "Plaid": "#3F8428", "Green": "#008066"} # Used in charts, from http://blog.richardallen.co.uk/uk-political-party-web-colours/

vote_share_data = {"Con": [32.4, 36.1, 36.8, 42.4], "Lab": [35.2, 29.0, 30.4, 40.0], "UKIP": [2.2, 3.1, 12.6, 1.8], "Lib Dem": [22.0, 23.0, 7.9, 7.4], "SNP": [1.5, 1.7, 4.7, 3.0], "Plaid": [0.6, 0.6, 0.6, 0.5], "Green": [1.0, 0.9, 3.6, 1.6]} # From https://en.wikipedia.org/wiki/United_Kingdom_general_election,_2017#Full_results, https://en.wikipedia.org/wiki/United_Kingdom_general_election,_2015#Results, https://en.wikipedia.org/wiki/United_Kingdom_general_election,_2010#Results, https://en.wikipedia.org/wiki/United_Kingdom_general_election,_2005#Results

vote_share = pd.DataFrame(data=vote_share_data, index=[2005, 2010, 2015, 2017])

def get_wiki(title): # Fetch wikitext
    url = "https://en.wikipedia.org/w/api.php"
    params = {
        "format": "json",
        "action": "parse",
        "prop": "wikitext",
        "redirects": True,
        "page": title
    }
    r = requests.get(url, params)
    return mwparserfromhell.parse(r.json()["parse"]["wikitext"]["*"])

def get_party(biog): # Get party from infobox's  'party' param
    intro = biog.get_sections()[0]
    templates = intro.filter_templates(matches=lambda node: "infobox" in node.name.lower())
    try:
        party = templates[0].get("party").value.filter_wikilinks(recursive=False)[-1].title # Ignore all but last party affiliation
    except Exception:
        party = pd.np.nan
    party_links = intro.filter_wikilinks(matches=lambda node: any(p == node.title for p in parties))
    party_links = [value.title.strip_code() for value in party_links]
    try:
        party_estimate = Counter(party_links).most_common(1)[0][0]
    except Exception:
        party_estimate = pd.np.nan
    return party, party_estimate

def parse_episodes(episodes): # Parse tables of episodes into list with entry for each panellist
    sections = episodes.get_sections(matches="\d{4}") # Episodes grouped by year
    data = []
    for table in sections:
        header_row = table.filter_tags(matches=lambda node: node.tag == "th")
        header = [value.contents.strip() for value in header_row]
        header += ["Panellist", "Party", "Party Estimate"]
        for row in table.filter_tags(matches=lambda node: node.tag == "tr")[1:-1]:
            data_row = row.contents.filter_tags(matches=lambda node: node.tag == "td")
            for panellist in data_row[4].contents.filter_wikilinks():
                print(panellist.title)
                biog = get_wiki(str(panellist.title))
                party, party_estimate = get_party(biog)
                date = data_row[2].contents.filter_text(recursive=False)[0].strip() # Remove refs from Airdate w/ filter_text()
                panellist_data = [value.contents.strip_code() for value in data_row] # Use strip_code for the other fields
                panellist_data[2] = date # Overwrite Airdate
                panellist_data += [panellist.title, party, party_estimate]
                data.append(panellist_data)
    return header, data

def party_group(party): # Group parties
    for group, needle in party_groups.items():
        if needle in party:
            return group

def get_data(): # Fetch wikitext, parse data, save to DataFrame and dump
    episodes = get_wiki(title="List_of_Question_Time_episodes")
    header, data = parse_episodes(episodes)
    data = pd.DataFrame.from_records(data, columns=header)
    data.to_csv("data.csv", index=False)
    return data

def summarise_data(): # Pivot data
    data = pd.read_csv("data.csv")
    data = data.loc[data["Airdate"] != "15 May 2001 Part 1"] # Drop odd ones
    data = data.loc[data["Airdate"] != "16 May 2001 Part 2"]
    data = data.loc[data["Party"].notnull()] # Drop non party-political panellists
    data["Airdate"] = data["Airdate"] = pd.to_datetime(data["Airdate"])
    data["Party Group"] = data["Party"].apply(party_group)
    data["Year"] = data["Airdate"].dt.year
    data["Month"] = data["Airdate"].dt.month
    annual = pd.pivot_table(data, index="Year", columns="Party Group", values="#", aggfunc="count")
    annual.fillna(value=0, inplace=True)
    annual = annual[annual.sum().sort_values(ascending=False).index]
    annual_pct = annual.apply(lambda x: x / x.sum(), axis=1)
    return annual_pct

def plot_stacked(annual_pct):
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(10,5))
    bottom = pd.Series([0 for val in annual_pct.index], index=annual_pct.index)
    charts = []
    for column in annual_pct.columns:
        ax.bar(annual_pct.index, annual_pct[column], bottom=bottom, color=party_colours[column], edgecolor="white", label=column)
        bottom = bottom + annual_pct[column]
    for edge in ["top", "bottom", "left", "right"]:
        ax.spines[edge].set_visible(False)
    ax.set_yticklabels(["{:.0%}".format(x) for x in ax.get_yticks()])
    lgd = fig.legend(loc="right", fontsize="small")
    ax.set_title("\nComposition of Question Time panels by party (excl. non party-political panellists)\n", fontsize=8)
    ax.tick_params(axis="both", which="major", labelsize="small")
    fig.savefig("stacked.png", dpi=150, bbox_extra_artists=(lgd,), bbox_inches="tight")

#get_data() # Run this once (takes several minutes) to create data.csv
annual_pct = summarise_data()
plot_stacked(annual_pct)
